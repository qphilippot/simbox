const configuration = require('./simbox.config');
const Entity = require('../types/entity');

class Simbox extends Entity {
    constructor(settings = {}) {
        super(settings);

        this.pool = {
            services: {},
            skills: {},
            controllers: {},
            ressources: {}
        };

        
        this.locations = {
            ressources: settings.locations.ressources,
            services: settings.locations.services,
            controllers: settings.locations.controllers,
            skills: settings.locations.skills
        };
    }
    
    static create(settings = {}) {
        const simbox = new Simbox(settings);
        simbox.initialize(settings);
        return simbox;
    }

    load(loader_path) {
        let loader = null;

        try {
            loader = require(loader_path);
            
            if (typeof loader === 'function') {
                loader(this);
            }
        }

        catch(error) {
            this.notifyError(error);
        }
    }

    resolveRessource(name) {
        let ressource = null;

        const token = name.split('-');

        if (token.length > 1) {
            const section = token.pop();
            const realName = token.join('-');
            
            if (section === 'controller') {
                this.getController(realName);
                return this.pool.ressources[name] || null;
            }


            else if (section === 'service') {
                this.getService(realName);
                return this.pool.ressources[name] || null;
            }
        }

        try {
            ressource = require(`${this.locations.ressources}${name}`);
        }

        catch(error) {
            this.notifyError(error);
        }

        finally {
            return ressource;
        }
    
    }

    setRessource(name, location) {
        try {
            const content = require(location);
            this.services.parser.recordFromContent(name, content);
            this.storeRessource(name, content);
        }

        catch(error) {
            this.notifyError(error);
        }
    }

    getRessource(name) {
        let ressource = null;

        if (typeof this.pool.ressources[name] !== 'undefined') {
            ressource = this.pool.ressources[name];
        }

        else {
            ressource = this.resolveRessource(name);
            if (ressource !== null) {
               this.storeRessource(name, ressource);
            }
        }

        return ressource;
    }

    storeRessource(name, ressource) {
        this.pool.ressources[name] = ressource;
    }

    getService(name, options = {}) {
        if (typeof this.pool.services[name] === 'undefined') {
            const Service = require(`${this.locations.services}${name}`);
            this.storeRessource(name + '-service', Service);
            options.app = this;
            const service = new Service(options);
            this.pool.services[name] = service;
        }

        return this.pool.services[name];
    }

    getController(name, options = {}) {
        if (typeof this.pool.controllers[name] === 'undefined') { 
            const Controller = require(`${this.locations.controllers}${name}`);
            this.storeRessource(name + '-controller', Controller);
            options.app = this;
            const controller = new Controller(options);
            this.pool.controllers[name] = controller;
        }

        return this.pool.controllers[name];
    }

    loadSkill(name, options = {}) {
        let Skill = null;
        try {
            Skill = require(`${this.locations.skills}${name}`);
        }

        catch(error) {
            this.notifyError(`
                Unable to load Skill @${name}\n
                Native error:${error.message}\n
                ${error.stack}
            `, true);
        }

        this.storeRessource(name + '-skill', Skill);
        options.app = this;
        const skill = new Skill(options);
        this.services.skills.record(name, skill);
    }

    existsSkill(name) {
        return this.services.skills.exists(name);
    }

    getSkill(name, options = {}) {
        if (!this.existsSkill(name)) { 
            try {
                this.loadSkill(name, options);
            }

            catch(error) {
                this.notifyError(`Unable to load skill: ${name} - (native message) ${error} - (native stack) ${error.stack}`, true);
            }
        }

        return this.services.skills.getById(name);
    }

    listSkills() {
        return Object.keys(this.services.skills.container).sort((a, b) => a.localeCompare(b));
    }


    listServices() {
        return Object.keys(this.pool.services).sort((a, b) => a.localeCompare(b));;
    }

    listController() {
        return Object.keys(this.pool.controllers).sort((a, b) => a.localeCompare(b));;
    }    

    listRessources() {
        return Object.keys(this.pool.ressources).sort((a, b) => a.localeCompare(b));;
    }

    setupRessources(settings = {}) {
        if (typeof settings.ressources) {
            const ressources = Object.keys(settings.ressources);
            ressources.forEach(name => {
                this.storeRessource(name, require(settings.ressources[name]));
            });
        }
    }

    initialize(settings = {}) {
        this.setApp(this);
        this.setupRessources(settings);
        this.setupServices(this.get('services_to_enable'));
    }

    record(className, classContent) {
        if (typeof classContent === 'string') {
            this.services.parser.recordFromLocation(className, classContent);
        }
    }

    create(ressourceName, settings = {}) {
        const ressource = this.getRessource(ressourceName);
        
        if (typeof settings.app === 'undefined') {
            settings.app = this;
        }

        return new ressource(settings);
    }
};

module.exports = Simbox.create(configuration);