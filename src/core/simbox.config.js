module.exports = {
    name: 'simbox',

    ressources: {
        skill: '../skills/skill'
    },

    services: {
        parser: 'parser',
        skills: 'skills'
    },

    locations: {
        ressources: '../types/',
        services: '../services/',
        controllers: '../controllers/',
        skills: '../skills/',
    },

    auto_setup_settings: false
};