const simbox = require('@simbox');
const Agent = simbox.getRessource('agent');

const configuration = require('./jobber.model.config');

class Jobber extends Agent {
    constructor(settings) {
        settings = Object.assign({}, configuration, settings);
        super(settings);

        this.launchSetupSkills(settings);
    }

    launchSetupSkills(settings) {
        const rules = this.core.rules['setup'];

        if (rules.length === 0) {
            return true;
        }
        
        rules.forEach(name => {
            return this.skills[name].initialize(settings);
        });
    }

    iniCoreData() {
        this.core = {
            rules: {
                setup: [],
                "input-check": [],
                "input-sync": [],
                trigger: [],
                "output-check": [],
                "output-outdated": [],
                "output-sending": []
            },

            lastOutput: {
                timestamp: null,
                value: null
            },

            pending_inputs: {},
            lastTrigger: null
        };
    }

    addRule(domain, skillName) {
        if (typeof this.core.rules[domain] !== 'undefined') {
            const skill = this.app.getSkill(skillName);

            if (skill === null) {
                this.notifyError(`
                    Unable to add rule ${domain}/${skillName}\n
                   
                `, true);

                return;
            }
 
            this.core.rules[domain].push(skillName);
            skill.attach(this);
        }

        else {
            this.notifyError(`Cannot set rules : ${domain} in Jobber. Candidats are : ${ Object.keys(this.core.rules) }`, true);
        }
    }

    onSetup(settings) {
        super.onSetup(settings);

        this.iniCoreData();
        this.onSetupRules(settings);
        this.onSetupMethodOverride(settings);
    }

    onSetupMethodOverride(settings) {
        if (typeof settings.override === 'undefined') {
            return;
        }

        const overridedMethods = settings.override;
        if (typeof overridedMethods.getResult === 'function') {
            this.getResult = overridedMethods.getResult.bind(this);
        }
    }

    addRulesFromObject(rulesObject) {
        Object.keys(rulesObject).forEach(domain => {
            rulesObject[domain].forEach(ruleName => {
                this.addRule(domain, ruleName);
            });
        });
    }

    onSetupRules(settings) {
        if (typeof settings.setRules === 'undefined') {
            this.addRulesFromObject(configuration.rules)

            if (typeof settings.useRules !== 'undefined') {
                this.addRulesFromObject(settings.useRules);
            }
        }

        else if (typeof settings.setRules !== 'undefined') {
            this.addRule(settings.setRules);
        }
    }
    
    async resolve_rules(domain, message) {
        const rules = this.core.rules[domain];

        if (rules.length <= 0) {
            return true;
        }
        
        const result = (await Promise.all(rules.map(name => {
            this.debug("resolve_rules", domain, name);
            
            let status = false;
            try {
                status = this.skills[name].check(message);
            } 

            catch(error) {
                this.notifyError(`${domain}-${name}-${error}`)
            }

            finally {
                return status;
            } 
        }))).every(test => test === true);

        // check if all rules checking return true
        return result;
    }

    async onNewMessage(message) {
        let continueProcess = true;
        
        continueProcess = await this.resolve_rules('input-check', message);
        if (continueProcess !== true) {
            return;
        }

        continueProcess = await this.resolve_rules('input-sync', message);
        if (continueProcess !== true) {
            return;
        }

        continueProcess = await this.resolve_rules('trigger', message);
        if (continueProcess !== true) {
            return;
        }

        else {
            this.job(message);
        }
    }

    getTimestamp() {
        return this.skills['message-same-timestamp'].timestamp;
    }

    // updateLastTrigger(message = { 
    //     getTimestamp: () => { return Date.now() }
    // }) {
    //     const timestamp = message.getTimestamp(); 
    //     if (timestamp >= this.core.lastTrigger) {
    //         this.core.lastTrigger = timestamp;
    //     }
    // }

    async job(message = {}) {
        if (typeof message.getTimestamp !== 'function') {
            message.getTimestamp = () => { return Date.now() }
        }

        try {
            const result = await this.onJob(message);

            
            this.clearInputs();
            this.clearPendingMessage();
            
            if (message !== null) {
                this.core.lastOutput.timestamp = message.getTimestamp();
            }

            // something was wrong with message, force correct timestamp
            if (!this.core.lastOutput.timestamp) {
                this.core.lastOutput.timestamp = Date.now();
            }

            this.core.lastOutput.value = result;
        
            if (await this.resolve_rules('output-check')) {
               this.propageResult(this.getResult(this.core.lastOutput.value));
            }

            else {
                this.notifyError('fail rules output');
            }
        }

        catch(e) {
            console.error(e);
            this.notifyError(e);
        }
    }

    clearInputs() {
        this.clearPendingMessage();
    }
    
    clearPendingMessage() {
        const messages = Object.values(this.core.pending_inputs);
        messages.forEach(message => {
            message.delete();
            delete this.core.pending_inputs[message.sender_id];
        });
    }

    getResult(lastOutput) {
        return this.core.lastOutput.value;
    }

    propageResult(results) {
        const rules = this.core.rules['output-sending'];

        this.debug('propageResult', rules);
        if (rules.length === 0) {
            return true;
        }
        
        rules.forEach(name => {
            return this.skills[name].send(results);
        });
    }

    async onJob(message) {
        return 8;
    }
}

module.exports = Jobber;