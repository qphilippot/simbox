const simbox = require('@simbox');
const SlotBox = simbox.getRessource('slot-box');

class DivideBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }

    checkSlotsConstraints(settings) {
        this.mustHaveInput('numerator');
        this.mustHaveInput('denominator');
        this.mustHaveOutput('result');
    }

    onJob(message) {
        const numerator = this.getInput('numerator');
        const denominator = this.getInput('denominator');

        let result = null;
        if (denominator === 0) {
            this.notifyError('Divide by 0 is forbidden');
        }

        else {
            result = numerator / denominator;
        }
        
        return result;
    }
}

module.exports = DivideBox;