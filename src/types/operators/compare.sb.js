const simbox = require('@simbox');
const SlotBox = simbox.getRessource('slot-box');

class CompareBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }
    
    checkSlotsConstraints(settings) {
        this.mustHaveInput('main');
        this.mustHaveInput('second');
        this.mustHaveOutput('result');
    }

    onJob(message) {
        const main = this.getInput('main');
        const second = this.getInput('second');

        let result = null;

        if (main > second) {
            result = 1;
        } 
        
        else if (main < second) {
            result = -1;
        }
        
        else {
            result = 0;
        }
        
        return result;
    }
}

module.exports = CompareBox;