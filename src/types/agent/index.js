const simbox = require('@simbox');
const Entity = simbox.getRessource('entity');
const Message = simbox.getRessource('message');

const AgentService = simbox.getService('agent');

class Agent extends Entity {
    constructor(settings = {}) {
        super(settings);
        this.checkOnNewMessageOverride(settings);
        this.setup(settings);
    }

    setupListener(settings) {
        this.on('newMessage', message_id => {
            this.debug('new message', message_id);
            this.onNewMessage(Message.getById(message_id));
        });
    }

    onNewMessage(message) {
        this.notifyError('non overrided on new message');
        // override me
        message.delete();
    }
    
    /** 
     * Allow override at creation without creating a new classe
     * Note : you probably should not use this function. 
     * */
    checkOnNewMessageOverride(settings) {
        // if (typeof settings.onNewMessage === 'function') {
        //     this.onNewMessage = settings.onNewMessage
        // }
    }

    setStatus(status) {
        this.status = status;
    }

    restoreContext(settings = {}) {
        this.context = {};
        
        if (typeof settings.context !== 'undefined') {
            Object.assign(this.context, settings.context);
        } 

        if (typeof this.context.slots !== 'undefined') {
            this.slots = this.context.slots;
        }
    }

    setup(settings) {
        // for some reason its usefull to may contact AgentService.
        // But try to not use. As soon as possible, please avoid to use it.
        // Businnes logic should not depends on services.
        this.setService('agent', AgentService);


        this.setupListener();
        this.type = settings.type;

        this.restoreContext(settings);        
        this.status = 'active';
        //Agent.store(this);
        this.onSetup(settings);
    }

    live() {
        this.onLive();
    }

    onLive() {
        // override me !
    }

    /** Override to do special things on setup */
    onSetup(settings) {}

    receiveMessage(message) {
        this.emit('newMessage', message._id);
    }

    sendTo(destinataire, message = {}) {
        if (typeof message === 'string') {
            message = Message.fromString(message);
        }

        // add timestamp if no timestamp wad set
        message.forceTimestamp();
        message.setReceiver(destinataire._id);
        message.setSender(this._id);

        destinataire.receiveMessage(message);
    }

    send(message = {}) {
        message.forceTimestamp();
        message.setSender(this._id);
        const destinataire = this.services.agent.getById(message.receiver_id);

        try {
            destinataire.receiveMessage(message);
        }

        catch(error) {
            this.notifyError(error, true);
        }
    }

    getData() {
        return {
            name: this.name,
            _id: this._id,
            type: this.type,
            context: this.context,
            status: this.status
        };
    }


}

Agent.pendingData = {};


module.exports = Agent;