const EventEmitter = require('events').EventEmitter;
const configuration = require('./entity.model.config');

let counter = 0;

class Entity {
    constructor(settings = {}) {
        // Must copy configuration file cause later a delete may remove services.skill
        settings = Object.assign(JSON.parse(JSON.stringify(configuration)), settings);
        
        this.name = settings.name;
        if (typeof settings.app !== 'undefined') {
            this.setApp(settings.app);
        } 

       
        this.event = new EventEmitter();

        this.initialize_skills(settings);
        

        this.skills = {};
        this.data = {};
        this.services = {};
        this.strictMode = settings.strictMode;
        this.verboseMode = settings.verboseMode;

        this.server_id = counter++;
        if (typeof settings._id === 'undefined') {
            this._id = 'server_id_' + this.server_id;
        }

        else {
            this._id = settings._id;
        }
        
        if (settings.testMode === true) {
            this.enableTestMode();
        }

        if (settings.auto_setup_settings === true) {
            this.setupServices(settings.services);
        }

        else {
            this.store('services_to_enable', settings.services);
        }
    }
    
    disable_skills(settings = {}) {
        if (typeof settings.services !== 'undefined') {
            delete settings.services.skill;
        }
    }

    initialize_skills(settings = {}) {
        this.skills = {};

        if (settings.disableSkills === true) {
            this.disable_skills(settings);
        }
    }

    setApp(app) {
        this.app = app;
    }

    setupServices(services = {}) {
        if (this.app) {
            Object.keys(services).forEach(name => {
                let service = null;
    
                try {
                    service = this.app.getService(services[name]); 
                }
    
                catch(error) {
                    this.notifyError(error);
                }
    
                finally {
                    if (service !== null) {
                        this.services[name] = service;
                    }
                }
            });
        }
    }

    setName(name) {
        this.name = name;
    }

    setService(serviceName, serviceInstance) {
        this.services[serviceName] = serviceInstance;
    }
    
    enableStrictMode() {
        this.strictMode = true;
    }

    disableStrictMode() {
        this.strictMode = false;
    }

    disableVerboseMode() {
        this.verboseMode = false;
    }

    enableVerboseMode() {
        this.verboseMode = true;
    }

    enableTestMode() {
        this.enableStrictMode();
        this.enableVerboseMode();
    }

    disableTestMode() {
        this.disableStrictMode();
        this.disableVerboseMode();
    }

    emit(eventName, data) {
        this.event.emit(eventName, data);
    }

    on(eventName, handler) {
        this.event.on(eventName, handler);
    }

    store(attributeName, attributeValue) {
        this.data[attributeName] = attributeValue;
    }

    get(attributeName) {
        return this.data[attributeName];
    }

    log() {
        console.log.call(null, this.name + ': ', ...arguments);       
    }

    debug() {
        if (this.verboseMode) {
            this.log(...arguments);
        }
    }

    doAsync(task) {
        setTimeout(() => {
            task();
        }, 1);
    }

    // async waitingFor(promisesArray) {
    //     return await Promise.all(promisesArray);
    // }

    hasSkill(skillName) {
        return typeof this.skills[skillName] !== 'undefined';
    }

    notifyError(error, throwError = true) {
        console.error(`[${this.name}] ${error}`);
        
        if (throwError === true || this.strictMode === true) {
            if (error instanceof Error) {
                throw error;
            }
            
            else {
                throw new Error(error);
            }
        }
    }

    die() {
        
    }
}

module.exports = Entity;