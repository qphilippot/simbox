const simbox = require('@simbox');
const Jobber = simbox.getRessource('jobber');
const Slot = simbox.getRessource('slot');

const defaultRules = {
    useRules: {
        setup: [ 'slot-io' ],
        'input-sync': ['slot-same-timestamp'],

        'input-check': [ 
            'only-alter-can-input-slots',
            'input-not-null'
        ],

        'output-outdated': ['output-outdated-resolve-backward'],

        trigger: [
            'all-actives-slots-must-be-ready', 
            'one-work-per-timestamp'
        ],
        
        'output-check': ['output-not-null'],

        

        'output-sending': [
            'forward-output-using-slot'
        ]
    }
};

class SlotBox extends Jobber {
    constructor(settings = {}) {

        super(Object.assign({}, defaultRules, settings));
        
        this.checkSlotsConstraints(settings);
    }

    checkSlotsConstraints(settings) {
        
    }

    mustHaveInput(name) {
        if (typeof this.core.slots.input[name] === 'undefined') {
            this.notifyError(`${this.constructor.name} must have an input-slot called "${name}"`);
        }
    }

    mustHaveOutput(name) {
        if (typeof this.core.slots.output[name] === 'undefined') {
            this.notifyError(`${this.constructor.name} must have an output-slot called "${name}"`);
        }
    }

    onJob() {
        return {
            data: 'hello'
        };
    }

    getInputActivesSlots() {
        return Object.values(this.core.slots.input).filter(slot => {
            return slot.active === true
        });
    }

    delete() {
        super.delete();

        const slots = this.core.slots;
        Object.value(slots.input).forEach(slot => slot.remove());
        Object.value(slots.output).forEach(slot => slot.remove());
    }

    getInputSlot(name) {
        const slot = this.core.slots.input[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no input slot called ${name}`);
            return null;
        }

        else {
            return slot;
        }
    }


    getOutputSlot(name) {
        const slot = this.core.slots.output[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no output slot called ${name}`);
            return null;
        }

        else {
            return slot;
        }
    }

    getInput(name) {
        const slot = this.core.slots.input[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no input slot called ${name}`);
            return null;
        }

        else {
            return slot.getValue();
        }


    }

    getResult(lastOutput) {
        // return same message to all slots
        let results = {}
        const slots = this.core.slots;
        Object.keys(slots.output).forEach(slotName => {
            results[slotName] = lastOutput;
        });

        return results;

        // or a custom response foreach
        // return {
        //     output_slot_name_a: results.value.foo,
        //     output_slot_name_b: results.value.b
        // };
    }

    clearInputs() {
        super.clearInputs();

        Object.values(this.core.slots.input).forEach(slot => {
            slot.clear();
        });
    }
    
    /**
     * check foreach slot their links, and regenerate link or unlink if needed
     */
    updateLinks() {
        const slots = this.core.slots;
        Object.values(slots.input).forEach((slot) => {
            Slot.restoreLink(slot);
        });

        Object.values(slots.output).forEach((slot) => {
            Slot.restoreLink(slot);
        });
    }

    onLive(settings) {
        super.onLive(settings);
        this.updateLinks();
    }

    static parse(document) {
        const slotBox = new SlotBox(document);
        return slotBox;
    }
}

module.exports = SlotBox;