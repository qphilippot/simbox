// todo : detect memory leak addind 'last use' property

const isStringAndNotEmpty = require('@tools/isStringAndNotEmpty');
const simbox = require('@simbox')
const Agent = simbox.getRessource('agent');
const Entity = simbox.getRessource('entity');
const AgentService = simbox.getService('agent');
const link_recorded = {};
const slotsByName = {};

class Slot extends Entity {
    constructor(settings = {}) {
        super(settings);
        this.active = settings.active || false; 
        this.link_id = settings.link_id || null;
        
        this.owner_id = settings.owner_id || null;
        this.alter_id = settings.alter_id || null;
        
        this.setName(settings.name);
        this.setType(settings.type);
        this.defaultValue = settings.defaultValue;
        
        this.value = null;
        this.timestamp = null;

        Slot.record(this);
    }

    getValue() {
        return this.isReady() ? this.value : this.defaultValue;
    }

    isReady() {
        return (
            this.active === true &&
            this.value !== null
        );
    }

    storeValue(value, timestamp = null) {
        this.value = value;
        this.timestamp = timestamp !== null ? timestamp : Date.now();
    }

    clear() {
        this.value = null;
    }

    getData() {
        return {
            active: this.active, 
            link_id: this.link_id,
            owner_id: this.owner_id,
            alter_id: this.id,
            name: this.name,
            type: this.type,
            defaultValue: this.defaultValue
        };
    }

    isLinkedWith(slot) {
        return (
            this.owner_id === slot.alter_id &&
            this.alter_id === slot.owner_id
        );
    }

    toString() {
        return JSON.stringify(this.getData);
    }

    link(slot = null, updatedSlots = new Set()) {
        if (slot === null) {
            throw new Error('cannot link a null-slot');
        }

        if (
            (this.type === 'input' && slot.type !== 'output') &&
            (this.type === 'output' && slot.type !== 'input')
        ) {
            throw new Error('incompatible type');
        }

        if (this.link_id !== null) {
            this.unlink(updatedSlots);
        }

        if (slot.link_id !== null) {
            slot.unlink(updatedSlots);
        }

        const link_id = Slot.generateLinkId(this, slot);

        this.link_id = link_id;
        slot.link_id = link_id;

        this.alter_id = slot.owner_id;
        slot.alter_id = this.owner_id;

        this.active = true;
        slot.active = true;


        let input, output;

        if (this.type === 'input') {
            input = this;
            output = slot;
        }

        else {
            input = slot;
            output = this;
        }

        updatedSlots.add(slot);
        updatedSlots.add(this);

        Slot.recordLink(output, input);
    }

    unlink(updatedSlots = new Set()) {
        if (Slot.existsLink(this.link_id)) {
            Slot.removeLink(this);
        }
        
        updatedSlots.add(this);
        this.active = false;
        this.alter_id = null;
        this.link_id = null;
    }

    remove() {
        Slot.remove(this);
    }

    checkLinkSanity() {
        const slots = Slot.findByLink(this.link_id);

        if (
            slots === null ||
            Object.keys(slots).length !== 2
        ) {
            return false;
        }

        const relatedSlotType = this.type === 'input' ? 'output' : 'input';
        const relatedSlot = slots[relatedSlotType];

        if (typeof relatedSlot === null) {
            return false;
        }

        if (
            !this.isLinkedWith(relatedSlot) ||
            this.active !== true ||
            relatedSlot.active !== true
        ) {
            return false;
        }

        return true;
    }

    setName(name = null) {
        if (!isStringAndNotEmpty(name)) {
            throw new Error('missing slot name');
        }
        
        this.name = name;
    }


    setType(type = null) {
        if (!isStringAndNotEmpty(type)) {
            throw new Error('missing slot name');
        }
        
        this.type = type;
    }


    static generateLinkId(output, input) {
        return `${output.owner_id}-${output.name}-${input.owner_id}-${input.name}`;
    }

    static record(slot) {
        if (typeof slotsByName[slot.name] === 'undefined') {
            slotsByName[slot.name] = [ this ];
        }

        else {
            slotsByName[slot.name].push(this);
        }
    }


    static recordLink(output, input) {
        const link_id = output.link_id;
        if (typeof link_recorded[link_id] === 'undefined') {
            link_recorded[link_id] = {
                input: input,
                output: output
            };
        }

        else {
            const link =  link_recorded[link_id]
            link.output = output;
            link.input = input;  
        }
    }

    static removeLink(slot = {}) {
        try {
            delete link_recorded[slot.link_id][slot.type];    
        }

        catch (error) {
            throw new Error(`Link not found : ${slot.link_id}`);            
        }

        if (Object.keys(link_recorded[slot.link_id]).length === 0) {
            delete link_recorded[slot.link_id];
        }
    }

    static remove(slot) {
        Slot.removeLink(slot);
        const arr = slotsByName[this.name];
        if (Array.isArray(arr)) {
            const index = arr.findIndex(slt => {
                return (
                    slt.link_id === this.link_id &&
                    slt.owner_id === this.owner_id
                );
            });

            // index === -1 => elt not found

            if (index >= 0) {
                arr.splice(index, 1);
            }
        }
    }

    static findByTypeAndLink(type, link_id) {
        if (typeof link_recorded[link_id] === 'undefined') {
            return null;
        }

        return link_recorded[link_id][type] || null;
    }

    static findByLink(link_id) {
        return link_recorded[link_id] || null;
    }

    static findByName(name) {

    }

    static listLink() {
        return Object.keys(link_recorded);
    }

    static listSlot() {
        return slotsByName;
    }

    static existsLink(link_id) {
        return Slot.findByLink(link_id) !== null;
    }

    static restoreLink(slot) {
        if (slot.link_id) {
            const alter = AgentService.getById(slot.alter_id);
            const alter_type = slot.type === 'input' ? 'output' : 'input';
            const alter_slot = Object.values(alter.core.slots[alter_type]).find(
                candidat => candidat.link_id === slot.link_id
            );

            if (!alter_slot) {
                slot.unlink();
            }
            
            if (!Slot.existsLink(slot.link_id)) {
                slot.link(alter_slot);
            }

            else {
                if (slot.checkLinkSanity() === false) {
                    slot.unlink();
                    slot.link(alter_slot);
                }
            }
        }
    }
}




module.exports = Slot;