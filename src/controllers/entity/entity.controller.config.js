module.exports = {
    name: 'entity-controller',
    errors: {
        entity_not_found: { 
            error : true, 
            message : 'not found'
        } 
    },

    services: {
        manager: 'entity'
    }
};