// use object-composition, not inheritancy
const Skill = require('../skill');

class ResolveBackward extends Skill {
	constructor() {
        super({
            name: 'output-outdated-resolve-backward'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(data) {
        this.debug('outdated', data);
        // retrieve all messages with outdated timestamp
        // const pending_inputs = Object.values(this.core.pending_inputs);
        // const inputs_outdated = pending_inputs.filter(pending_message => {
        //     return pending_message.getTimestamp() === timestamp;
        // });

        const inputs_outdated = data.searchOutdated(this, data.timestamp);

        if (inputs_outdated.length === 0) {
            this.job({
                getTimestamp: () => { return data.timestamp; }
            });
        }

        // propage backward
        else {

            this.debug('i will propage to', inputs_outdated);
            // inputs_outdated.forEach(outaded_message => {
            //     const jobber = Agent.getById(outaded_message.getSender());
            //     jobber.resolve_rules('output_outdated', timestamp);
            // });

            inputs_outdated.forEach(outdated => {
                data.propage(outdated);
            });
        }
    }
}

module.exports = ResolveBackward;