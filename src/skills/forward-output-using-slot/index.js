// use object-composition, not inheritancy
const simbox = require('@simbox');
const Message = simbox.getRessource('message');
const Skill = simbox.getRessource('skill');

const skillName = 'forward-output-using-slot';
const version = '1.0';

class OuputSlots extends Skill {
	constructor() {
        super({
            name: skillName,
            version: version
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
            send: this.send.bind(instance),
            version: this.version
        }
	}

    send(results) {
        if (this.hasSkill('slot-io') === false) {
            this.notifyError(`${skillName} require @skill:slot-io to run`, true);
            return false;
        }

        const output_slots =  this.core.slots.output;

        if (Object.keys(output_slots).length <= 0) {
            return;
        } 

        Object.keys(results).forEach(slotName => {
            const slot =  output_slots[slotName];
            if (typeof slot === 'undefined') {
                this.notifyError(`@${skillName} output-slot not found : ${slotName}`);
                return;
            }

            this.debug('send message with timestamp', this.core.lastOutput.timestamp, this.core.lastOutput.value);

            const message = new Message({
                content: {
                    link_id: slot.link_id,
                    value: results[slotName]
                },

                timestamp: this.core.lastOutput.timestamp,
                sender_id: this._id,
                receiver_id: slot.alter_id
            });

            this.send(message);
        });
    }
}

module.exports = OuputSlots;