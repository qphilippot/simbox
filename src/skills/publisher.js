// use object-composition, not inheritancy
const Skill = require('./skill');

const skillName = 'publisher';
class Publisher extends Skill {
	constructor(settings = {}) {
		settings = Object.assign({}, { name: skillName }, settings);
		super(settings);
	}

	attach(instance) {
		instance.skills[this.name] = {
			receivers: [],
			send: this.send.bind(instance),
			isRecipient: this.isRecipient.bind(instance),
			addReceiver: this.addReceiver.bind(instance),
			removeReceiver: this.removeReceiver.bind(instance)
		};
	}

	send(message = {}) {
		const skill = this.skills.publisher;
		message.setSender(this._id);
		
		Object.values(skill.receivers).forEach(receiver => {
			// create a real new message for each receivers
			const personnalizedMessage = message.copy();
			personnalizedMessage.setReceiver(receiver._id);
			this.send(personnalizedMessage);
		});

		// don't forget to delete model message
		message.delete();
	}

	isRecipient(entity_id) {
		const collection = this.skills.publisher.receivers;
		return typeof collection[entity_id] !== 'undefined';
	}

	addReceiver(entity = null) {
		if (entity === null) {
			this.notifyError('unable to add receiver, entity is null');
		}


		const context = this.skills.publisher;
		if (!context.isRecipient(entity._id)) {
			context.receivers[entity._id] = entity;

			if (entity.hasSkill('receiver')) {
				entity.skills.receiver.register(this);
			}
		}
	}
	
	removeReceiver(entity) {
		const context = this.skills.publisher;
		if (context.isRecipient(entity._id)) {
			delete context.receivers[entity._id];

			if (entity.hasSkill('receiver')) {
				entity.skills.receiver.unregister(this);
			}
		}
	}	
}

module.exports = Publisher;