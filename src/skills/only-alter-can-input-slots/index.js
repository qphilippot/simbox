// use object-composition, not inheritancy
const simbox = require('@simbox');

const Skill = require('../skill');
const Slot = simbox.getRessource('slot');

const skillName = 'only-alter-can-input-slots';
class AllowedSlots extends Skill {
	constructor() {
        super({
            name: skillName,
            version: '1.0'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
            check: this.check.bind(instance),
            version: this.version
        }
	}

    check(message = {}) {
        const link_id = message.content.link_id;
        const slot = Slot.findByTypeAndLink('input', link_id);

        if (slot === null) {
            this.notifyError(`@${skillName} slot not found using link : ${link_id}`);
            return false;
        }

        if (
            typeof slot === 'undefined' ||
            slot === null ||
            slot.checkLinkSanity(link_id) === false
        ) {
            this.notifyError(`@${skillName} unsane link : ${link_id}`);
            return false;
        }


        if (slot.alter_id !== message.sender_id) {
            this.notifyError(`@${skillName} slot-output is not sender - fake message detected in slot system. (link_id: ${link_id}, sender_id: ${message.sender_id}, agent : ${this._id}, alter_id: ${slot.input.alter_id})`);    
            return false;
        }

        slot.storeValue(message.content.value, message.getTimestamp());
        return true;
    }
}

module.exports = AllowedSlots;