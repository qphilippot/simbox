// use object-composition, not inheritancy
const Skill = require('../skill');
const configuration = require('./input-not-null.skill.config');

class NotNull extends Skill {
	constructor(settings = {}) {
        settings = Object.assign({}, configuration, settings);
        super(settings);
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(message = {}) {
        if (message.isEmpty()) {
            this.notifyError(`INPUT_CHECKING::NOT_NULL failure (${message})`, true);
            return false;
        }

        else {
            return true;
        }
    }
}

module.exports = NotNull;