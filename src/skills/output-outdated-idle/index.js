// use object-composition, not inheritancy
const Skill = require('../skill');

class Idle extends Skill {
	constructor() {
        super({
            name: 'output-outdated-idle'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(timestamp) {
        this.core.lastOutput.timestamp = timestamp;
        this.propageResult();
    }
}

module.exports = Idle;