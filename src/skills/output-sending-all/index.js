// use object-composition, not inheritancy
const Skill = require('../skill');
const simbox = require('@simbox');
const Message = simbox.getRessource('message');

class All extends Skill {
	constructor() {
        super({
            name: 'output-sending-all'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			send: this.send.bind(instance)
        }
	}

    async send(timestamp) {
        const message = new Message({
            timestamp: this.core.lastOutput.timestamp,
            content: { ...this.core.lastOutput.value },
            sender_id: this._id
        });

        if (this.hasSkill('publisher')) {
            this.skills.publisher.publish(message);
        }

        else {
            this.notifyError('I dont know how to send my data');
        }
    }
}

module.exports = All;