// use object-composition, not inheritancy
const Skill = require('../skill');

class HardWorker extends Skill {
	constructor() {
        super({
            name: 'always-trigger'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(message = {}) {
        return true;
    }
}

module.exports = HardWorker;