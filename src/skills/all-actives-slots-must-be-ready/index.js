// use object-composition, not inheritancy
const Skill = require('../skill');
const simbox = require('@simbox');
const Slot = simbox.getRessource('slot');

const skillName = 'slot-io';
class SlotSystem extends Skill {
	constructor() {
        super({
            name: skillName,
            version: '1.0'
        });
	}

	attach(instance) {
		instance.skills[skillName] = {
            initialize: this.initialize.bind(instance),
            retrieveSlotsFromContext: this.retrieveSlotsFromContext.bind(instance),
            version: this.version
        }
	}

    initialize(settings = {}) {
        const skill = this.skills[skillName];
        if (typeof this.core.slots === 'undefined') {
            this.core.slots = {
                input: {},
                output: {}
            };
        }

        if (typeof settings.context !== 'undefined') {
            skill.retrieveSlotsFromContext(settings.context.slots);
        }

        if (typeof settings.slots !== 'undefined') {
            skill.retrieveSlotsFromContext(settings.slots);
        }
    }

    retrieveSlotsFromContext(slots = {}) {
        if (typeof slots.input !== 'undefined') {
            Object.keys(slots.input).forEach(slot_name => {
                const slotData = slots.input[slot_name];
                slotData.owner_id = this._id;
                slotData.type = 'input';
                slotData.name = slotData.name || slot_name;
                this.core.slots.input[slot_name] = new Slot(slotData);
            });
        }

        if (typeof slots.output !== 'undefined') {
            Object.keys(slots.output).forEach(slot_name => {
                const slotData = slots.output[slot_name];
                slotData.owner_id = this._id;
                slotData.type = 'output';
                slotData.name = slotData.name || slot_name;
                this.core.slots.output[slot_name] = new Slot(slotData);
            });
        } 
    }
}

module.exports = SlotSystem;