// use object-composition, not inheritancy
const Skill = require('../skill');
const isObjectEmpty = require('@tools/isObjectEmpty')

class NotNull extends Skill {
	constructor() {
        super({
            name: 'output-not-null'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check() {
        if (isObjectEmpty(this.core.lastOutput.value)) {
            this.notifyError(`value : ${this.core.lastOutput.value} is forbidden by rule OUTPUT_CHECKING::NOT_NULL`);
            return false;
        }

        else {
            return true;
        }
    }
}

module.exports = NotNull;