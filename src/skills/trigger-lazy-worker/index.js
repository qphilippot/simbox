// use object-composition, not inheritancy
const Skill = require('../skill');

class LazyWorker extends Skill {
	constructor() {
        super({
            name: 'trigger-lazy-worker'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }

        // waiting for this event
        this.on('messageFromAllRegisters', () => {
            this.job(message);
        });
	}

    async check(message = {}) {
        // never check when we receive one message
        return false;
    }
}

module.exports = LazyWorker;