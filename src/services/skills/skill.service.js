const EntityService = require('../entity');
const configuration = require('./skill.service.config');
const simbox = require('@simbox');

class SkillService extends EntityService {
    constructor(settings = {}) {
        settings = Object.assign({}, configuration, settings);
        super(settings);
    }
}


module.exports = SkillService;
