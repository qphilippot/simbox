const EntityService = require('../entity');

class AgentService extends EntityService {
    constructor(settings = {}) {
        super({
            name: `AgentService`,
            ...settings
        });
    }
    

    onCreateNewInstanceInDB(agent, settings) { /*      ;)      */
        agent.name = settings.name || 'anonymous';
		agent.created_at = Date.now(); 
		agent.status = "sleep";
		agent.inner = [];
		agent.outer = [];
        agent.context = typeof settings.context === 'string' ? JSON.parse(settings.context) : settings.context;
        agent.type = settings.type;
        agent.society = settings.society;
    }

    async updateStatus(agent_id, status) {
        let agent_data = await this.retrieveById(agent_id);
        if (
            agent_data !== null && 
            agent_data.status !== status
        ) {
            agent_data.status = status;			
            agent_data = await agent_data.save();

            const agent = this.getById(agent_data._id);
            const newStatus = agent_data.status; 
            
            if (newStatus === 'active') {
                // instance is currently loaded in server
                if (typeof agent !== 'undefined') {
                    agent.status = agent_data.status;
                } 

                // instance is not loader, we have to load her
                else {
                    await this.downloadById(agent_data._id);
                }
            }

            // remove instance from server
            if (
                newStatus === 'sleep' && 
                typeof agent !== 'undefined'
            ) {
                this.killById(agent._id);
            }
        }
        
        return agent;
    }

    async launch(agent = {}) {
        if (agent === null) {
            return null;
        }

        else {
            return agent.live();
        }
        
    }
    // static replaceSlot(agent, type, key, slot) {
    //     const lastState = JSON.parse(JSON.stringify(agent.slots[type][key]));
    //     delete agent.slots[type][key];
    //     const entry = agent.slots[type];
    //     // search in all input / output slot if from / to agent is still connected
    //     if (type === 'input') {
    //         const slotsName = Object.keys(entry);
    //         let stillHere = false;
    //         slotsName.forEach(name => {
    //             const s = entry[name];
    //             if (s.from === lastState.from) {
    //                 stillHere = true;
    //             } 
    //         });

    //         if (stillHere === false) {
    //            agent.inner.delete(lastState.from); 
    //         }

    //         agent.inner.add(slot.from);
    //     }

    //     if (type === 'output') {
    //         const slotsName = Object.keys(entry);
    //         let stillHere = false;
    //         slotsName.forEach(name => {
    //             const s = entry[name];
    //             if (s.to === lastState.to) {
    //                 stillHere = true;
    //             } 
    //         });

    //         if (stillHere === false) {
    //             agent.outer.delete(lastState.to);
    //         }
    //     }

    //     agent.slots[type][key] = slot;

    //     return agent;
    // }

    // static async updateSlot(agent_id, type, key, slot) {
    //     // update slot instance
    //     const instance = replaceSlot(Agent.getById(agent_id), type, key, slot);
    //     const agentData = await AgentController.getById(agent_id);
        
    //     agentData.inner = Array.from(instance.inner);
    //     agentData.outer = Array.from(instance.outer);
        
    //     if (typeof agentData.context.slots[type] === 'undefined') {
    //         agentData.context.slots[type] = {};
    //     }

    //     agentData.context.slots[type][key] = slot;

    //     agentData.markModified('context');
    //     return await agentData.save();
    // }



    async updateContext(agent_id, property, value) {
        let agent_data = null;
        try {
			agent_data = await this.retrieveById(agent_id);
		}

		catch(error) {
            this.notifyError(error, true);
		}
        
		if (agent_data !== null) {

            // really update in DB
            if (typeof agent_data.context === 'string') {
                const str = agent_data.context;
                agent_data.context = {
                    string: agent_data.context
                };
            }

            agent_data.context[property] = value;
            
            
            try {
                agent_data.markModified('context');
				await agent_data.save();
			}

			catch(error) {
                this.notifyError(error, true);
            }
            
            // update name in server agent pool
            let agent = this.getById(agent_id); 
            if (agent) {
                agent.context = agent_data.context;
            }

            return agent_data;
        }

        else {
            this.notifyError(error, true);
        }
    }
    
   
    async leaveSociety(agent_id) {
        let agent_data = await this.retrieveById(agent_id);

        if (agent_data) {
            agent_data.society_id = null;
        }

        else {
            this.notifyError(
                new Error(`agent not found in db with { _id: ${agent_id}`), 

                {
                    title: 'Agent Controller Error',
                    message: `Try to update agent with bad id (${society_id})`,
                    tags: [ 'objectID', 'mongoose', 'agent', 'update', 'addToSocietyInDB', 'controller' ]
                }
            );
        }

        await agent_data.save();

        if (this.getById(agent_id)) {
            this.society_id = null;
        }
    }

    async setSociety(agent_id, society_id) {
        let agent_data = await this.retrieveById(agent_id);

        if (agent_data) {
            agent_data.society_id = society_id;
        }

        else {
            this.notifyError(
                new Error(`agent not found in db with { _id: ${agent_id}`), 

                {
                    title: 'Agent Controller Error',
                    message: `Try to update agent with bad id (${society_id})`,
                    tags: [ 'objectID', 'mongoose', 'agent', 'update', 'addToSocietyInDB', 'controller' ]
                }
            );
        }

        await agent_data.save();
    }


    // static async removeInnerLiaisonWith(agent_id) {
    //     const receivers = await AgentDB.find({ inner: agent_id });
        
    //     receivers.forEach(async (receiver) => {
    //         const index = receiver.inner.indexOf(agent_id);
    //         if (index >= 0) {
    //             receiver.inner.splice(index, 1);
    //             await receiver.save();
    //             const instance = Agent.getById(receiver._id); 
    //             if (instance) {
    //                 instance.removeInner(agent_id);
    //             }
    //         }
    //     });
    // }

    // static async removeOuterLiaisonWith(agent_id) {
    //     const publishers = await AgentDB.find({ outer: agent_id });

    //     publishers.forEach(async (publisher) => {
    //         const index = publisher.outer.indexOf(agent_id);
    //         if (index >= 0) {
    //             publisher.outer.splice(index, 1);
    //             await publisher.save();
    //             const instance = Agent.getById(publisher._id); 
    //             if (instance) {
    //                 instance.removeOuter(agent_id);
    //             }
    //         }
    //     });
    // }

    async updateType(agent_id, type) {
        let agent_data = null;
        
        try {
            agent_data = await this.retrieveById(agent_id);
        }

        catch(error) {
            this.notifyError(error, true);
        }

        if (agent_data !== null) {
            agent_data.type = type
            return await agent_data.save();
        }

        else {
            return agent_data;
        }
    }

    // static async removeFromSociety(agent_id) {
    //     const agent = await AgentController.getById(agent_id);
    //     agent.society = null;
    //     await agent.save();


    //     const instance = AgentController.retrieveInstance(agent_id);
    //     if (instance) {
    //         instance.society = null;
    //     }
    // }

    // static addOuter(agent_id, outer_id) {
    //     // todo add redundance test
    //     // todo update db
    //     const agent = Agent.getById(agent._id);
    //     const outer = Agent.getById(outer_id);

    //     if (agent && outer) {
    //         agent.addOuter(outer);
    //         outer.addInner(agent);
    //     }
    // }

    // static addInner_instance(agent_id, inner_id) {
    //     const agent = Agent.getById(agent._id);
    //     const inner = Agent.getById(inner_id);

    //     if (agent && inner) {
    //         agent.addInner(inner);
    //         inner.addOuter(agent);
    //     }
    // }

    // static async removeInner(agent_id, inner_id) {
    //     const agent = await AgentDB.findById(agent_id);
    //     const index = agent.inner.indexOf(inner_id);

    //     if (index >= 0) {
    //         agent.inner.splice(index, 1);
    //         await agent.save();
    //         const instance = Agent.getById(agent._id); 
    //         if (instance) {
    //             instance.removeInner(inner_id);
    //         }
    //     }
    // }

    // static async removeOuter(agent_id, outer_id) {
    //     const agent = await AgentDB.findById(agent_id);
    //     const index = agent.outer.indexOf(outer_id);

    //     if (index >= 0) {
    //         agent.outer.splice(index, 1);
    //         await agent.save();
    //         const instance = Agent.getById(agent._id); 
    //         if (instance) {
    //             instance.removeOuter(outer_id);
    //         }
    //     }
    // }
}


module.exports = AgentService;
