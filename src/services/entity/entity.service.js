const Entity = require('../../types/entity');
const configuration = require('./entity.service.config');

class EntityService extends Entity {
    constructor(settings = {}) {
        settings = Object.assign({}, configuration, settings);
        super(settings);

        this.services.parser = this.app.getService('parser');
        this.container = {};
        this.pendingLoading = {};
        this.model = settings.model || null;
    }

    async launchAllById(entity_id_array) {
        try {
            await Promise.all(
                entity_id_array.map(id => this.launch(this.getById(id)))
            );
        }

        catch(error) {
            console.error(error);
        }
    }

    getByName(name) {
        return Object.values(this.container).filter(entity => entity.name === name);
    }
    
    async retrieveByName(name) {
        return this.model.find({ name: name });
    }

    getById(entity_id) {
        return this.container[entity_id] || null;
    }

    getAll() {
        return this.container;
    }

    onError(error) {
        console.error(error);
    }

    async retrieveAll() {
        if (this.model !== null) {
            let document = null;

            try {
                document = await this.model.find({});
            }

            catch(error) {
                this.onError(error);
            }

            finally {
                return document;
            }
        }

        else {
            throw new Error('Model is null');
        }
    }

    async retrieveById(entity_id) {
        if (this.model !== null) {
            let document = null;

            try {
                document = await this.model.findById(entity_id);
            }

            catch(error) {
                this.onError(error);
            }

            finally {
                return document;
            }
        }

        else {
            throw new Error('Model is null');
        }
    }

    async rename(entity_id, name) {
        let entity_data = await this.retrieveById(entity_id);
        
        if (entity_data !== null) {
			entity_data.name = name;
            
            try {
				await entity_data.save();
			}

			catch(error) {
				this.notifyError(error);
            }
        }

        // update instance name in server pool
        const instance = this.getById(entity_id);
        
        if (instance !== null) {
            instance.setName(name);
        }
    }

    isLoading(entity_id) {
        return typeof this.pendingLoading[entity_id] !== 'undefined';
    }

    exists(entity_id) {
        return this.getById(entity_id) !== null;
    }

    onLoadStart(entity_id) {
        this.pendingLoading[entity_id] = true;
    }

    onLoadEnd(entity_id) {
        delete this.pendingLoading[entity_id];
    }

    getEmptyInstance() {
        return  new (this.model)();
    }

    parse(settings = {}) {
        const parser = this.services.parser.get(settings.type);
        
        if (typeof parser !== 'function') {
            throw new Error(`Parser not found for type ${settings.type}`);
        }

        else {
            const entity = parser(settings);
            return entity;
        }
    }

    onCreateNewInstanceInDB(entity, settings = {}) {

    }

    async createInDB(settings) {
        const entity = this.getEmptyInstance();
        this.onCreateNewInstanceInDB(entity, settings);
        
        
        let data = {};

        try {
            data = await entity.save();
        }

        catch (error) {
            this.notifyError(error);
            data = error;
        }

        finally {
            return data;
        }
    }

    async downloadAll() {
        return await this.download({});
    }

    async downloadActives() {
        return await this.download({ status : 'active' });
    }

    async download(filter) {
        const collection = await this.model.find(filter);
    
        if (collection !== null) {
            return collection.map(document => {
                try {
                    this.createInstanceFromJSON(document);
                }

                catch(error) {
                    console.error(error);
                }
                
            });
        }

        else {
            return [];
        }
    }

    list() {
        return Object.keys(this.container);
    }

    async launchById(entity_id) {
        const entity = this.getById(entity_id);
        await this.launch(entity);
    }

    async launchAll() {
        await Promise.all(Object.values(this.container).map(entity => 
            this.launch(entity)
        ));
    }

    async launch(entity) {
        // @override me ! 
        console.error('error');
        this.debug('use not overrided Entity.prototype.launch method', entity);   
    }

    createInstanceFromJSON(json_data = null) {
        let entity = null;

        if (typeof json_data.type === 'undefined') {
            json_data.type = this.data.type;
        }

        if (json_data !== null) {
            try {
                entity = this.parse(json_data);
            }

            catch(error) {
                this.notifyError(error);
            }

            if (entity !== null) {
                this.recordInstance(entity);
            }

            else {
                console.error('entity is null')
            }
        }

        return entity;
    }
    
    async downloadById(entity_id) {
        if (
            this.exists(entity_id) ||
            this.isLoading(entity_id)
        ) {
            return null;
        }

        this.onLoadStart(entity_id);
    
        const entity_data = await this.retrieveById(entity_id);
        const entity = this.createInstanceFromJSON(entity_data);

        this.onLoadEnd(entity_id);
        return entity;
    }

    async loadMultiples(arrayOfID) {
        let agents = await Promise.all(arrayOfID.map(id => this.downloadById(id)));
        return agents;
    }

    async delete(entity_id) {
        const entity_data = await this.retrieveById(entity_id);
        
        if (entity_data === null) {
            this.notifyError(`no entity found with id: ${entity_id}`, true);
        }

        await this.onDelete(entity_data);
        
        if (entity_data) {
            await this.removeInDB(entity_id);
        }

        this.killById(entity_id);
        
        return 'done';
    }


    async onDelete(entity_data) {

    }

    async removeInDB(entity_id) {
        try {
            await this.model.deleteOne({ _id: entity_id });
        }

        catch(error) {
            this.notifyError(error, {
                title: this.name,
                message: `Error when trying to remove entity in db with id : (${entity_id})`,
                tags: [
                    'objectID',
                    'mongoose',
                    'entity',
                    'delete',
                    'removeInDB',
                    'service',
                    this.name
                ]
            });
        }
    }
    
    killById(entity_id) {
        const entity = this.getById(entity_id);
        if (entity !== null) {
            entity.die();
            delete this.container[entity_id];
        } 
    }

    async createAndStore(settings) {
        const data = await this.createInDB(settings);
        
        try {
            this.create(data);
        }

        catch(error) {
            this.notifyError(error);
        }

        return data;
    }

    create(settings = {}) {
        if (typeof settings.app === 'undefined') {
            settings.app = this.app;
        }
        
        const entity = this.parse(settings);
        this.recordInstance(entity);

        return entity;
    }

    record(key, content) {
        this.container[key] = content;
    }

    recordInstance(entity) {
        this.container[entity._id] = entity;
    }
}

module.exports = EntityService;